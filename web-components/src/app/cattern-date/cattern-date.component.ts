import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'cattern-date',
    templateUrl: './cattern-date.component.html',
    styleUrls: ['./cattern-date.component.css'],
    encapsulation: ViewEncapsulation.ShadowDom
})

export class CatternDateComponent implements OnInit {
    @Input() unix_datetime: string;
    @Input() field_id: string;
    @Input() field_name: string;
    @Input() field_label: string;
    formatted_date: string;
    db_date_updated: string;
    error_message: string;
    error_found: boolean;
    button_style: string;
    unique_id: string;

    constructor() {
    }

    ngOnInit() {
        this.unique_id = this.field_id;
        this.error_message = 'unfinished Typescript code.';
        this.error_found = false;
        this.button_style = 'valid_date';
        this.db_date_updated = '';
        this.parseUnixDateTime();
    }

    parseUnixDateTime(): void {
        const halves = this.unix_datetime.split(' ');

        // Is the field empty?
        if (halves[0] === '') {
            this.formatted_date = '';
            this.db_date_updated = '';
            this.button_style = 'valid_date';
            this.error_message = 'Empty';
            return null;
        }

        // parse the date
        const date_pieces = halves[0].split('-');
        this.formatted_date = date_pieces[1] + '/' + date_pieces[2] + '/' + date_pieces[0];
    }

    is_empty(empty_string): void {
        if ( empty_string === '' ) {
            this.button_style = 'valid_date';
            this.error_found = true;
            this.error_message = 'Date is required.';
            this.db_date_updated = '';
        }
    }

    validate_date(edited_string): void {
        let date_pieces = [0, 0, 0];
        const re = /\//gi;
        if (edited_string.search(re) === -1 ) {
            this.button_style = 'valid_date';
            this.error_found = true;
            this.error_message = 'Date mm/dd/yyyy is required.';
            this.db_date_updated = '';
        } else {
            date_pieces = edited_string.split('/');
            date_pieces[1] = ( date_pieces[1] === undefined ) ? 0 : date_pieces[1] ;
            date_pieces[2] = ( date_pieces[2] === undefined ) ? 0 : date_pieces[2] ;
            this.button_style = 'valid_date';
            this.error_found = false;

            if ( date_pieces[0] < 1 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Month is required.';
                this.db_date_updated = '';
            } else if ( date_pieces[1] < 1 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Day is required.';
                this.db_date_updated = '';
            } else if ( date_pieces[0] > 12 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Month is invalid.';
                this.db_date_updated = '';
            } else if ( date_pieces[1] > 31 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Day is invalid.';
                this.db_date_updated = '';
            } else if ( date_pieces[2] < 1 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Year is required.';
                this.db_date_updated = '';
            } else if ( date_pieces[2] > 2019 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Year is invalid.';
                this.db_date_updated = '';
            } else if ( date_pieces[2] < 1900 ) {
                this.button_style = 'valid_date';
                this.error_found = true;
                this.error_message = 'Year is invalid.';
                this.db_date_updated = '';
            }

            if ( this.error_found === false ) {
                this.db_date_updated = date_pieces[2]
                    + '-' + date_pieces[0].toString().padStart(2, '0')
                    + '-' + date_pieces[1].toString().padStart(2, '0')
                    + ' 00:00:00';
            }
        }
    }
}
