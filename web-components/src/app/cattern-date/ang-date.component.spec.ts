import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatternDateComponent } from './cattern-date.component';

describe('AngInTextComponent', () => {
  let component: CatternDateComponent;
  let fixture: ComponentFixture<CatternDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatternDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatternDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
