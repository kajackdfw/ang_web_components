import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { CatternButtonComponent } from './cattern-button/cattern-button.component';
import { CatternDateComponent } from './cattern-date/cattern-date.component';

@NgModule({
    declarations: [
        CatternButtonComponent,
        CatternDateComponent,
    ],
    imports: [
        BrowserModule
    ],
    entryComponents: [
        CatternButtonComponent,
        CatternDateComponent
    ],
    providers: [],
})
export class AppModule {
    constructor(injector: Injector) {

        const acc2 = createCustomElement(CatternButtonComponent, { injector });
        customElements.define('cattern-button', acc2);

        const acc3 = createCustomElement(CatternDateComponent, { injector });
        customElements.define('cattern-date', acc3);
    }
    ngDoBootstrap() {}
}
