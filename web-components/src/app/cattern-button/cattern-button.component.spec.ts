import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatternButtonComponent } from './cattern-button.component';

describe('AngViewerComponent', () => {
  let component: CatternButtonComponent;
  let fixture: ComponentFixture<CatternButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatternButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatternButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
