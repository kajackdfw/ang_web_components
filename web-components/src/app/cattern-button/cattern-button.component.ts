import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'cattern-button',
    templateUrl: './cattern-button.component.html',
    styleUrls: ['./cattern-button.component.css'],
    encapsulation: ViewEncapsulation.ShadowDom
})

export class CatternButtonComponent implements OnInit {
    @Input() colorize: string;
    cssColor: string;
    displayText: string;
    constructor() { }

    ngOnInit() {
        if (this.colorize == null) {
            this.cssColor = 'empty';
            this.displayText = 'Empty color value is boring.';
        } else if (this.colorize === 'red' || this.colorize === 'blue') {
            this.cssColor = this.colorize;
            this.displayText = 'This button is ' + this.colorize + ' colored.';
        } else {
            this.cssColor = 'undefined';
            this.displayText = this.colorize + ' color is not supported.';
        }
    }
}
