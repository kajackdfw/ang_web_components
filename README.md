# ang_web_components

# Description
This is a starter setup for authoring portable web components with Angular, and NOT about writing an Angular single page 
application. If you have an old legacy website written in a server side language, you can cleanup your html with web 
components with out touching your business logic on the back end. Web components can display validation messages on the
page before submits, and/or do data conversions for display. See how the cattern-date web component accepts a unix date 
time string, displays a user friendly date, and returns a unix date time in the submit.

# After Forking This Repo
1) >cd web-components
2) >npm install
3) >ng build --prod --output-hashing=none

If the last step completed with out errors, you are ready to add your own component. I recommend you leave the catter- components in until you get your new component working.

# Building this repo from scratch
Build this dev environment from scratch using ng CLI
1) >ng new web-components 
2) >cd web-components
3) >ng add @angular/elements
4) >cd src
5) >rm app.component.ts
6) >rm app.component.html
5) >ng g component cattern-button
6) >Add content to cattern-button.component.ts
5) >ng g component cattern-date
6) >Add content to cattern-date.component.ts
7) >Add the components to app.module.ts
8) >Write the contents of cattern-date and cattern-button
9) >ng build --prod --output-hashing=none
10) >Copy the contents of /dist to your web server

If you load the sample page index.html and you see **TypeError: Constructor HTMLElement requires 'new'**, roll back the document register package version with ....

1) >npm i document-register-element@1.8.1
2) >npm audit -fix --force

#Todos
try out the end to end test
